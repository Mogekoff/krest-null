from dataclasses import dataclass, asdict
from enum import Enum, IntEnum, auto
from datetime import datetime
import tzlocal
import json
import upnpclient

def getExternalAddress():
    devices = upnpclient.discover()
    externalIP = devices[0].WANIPConn1.GetExternalIPAddress()
    return externalIP['NewExternalIPAddress']


class MsgType(IntEnum):
    CON = auto()
    BYE = auto()
    REG = auto()
    OK = auto()
    FAIL = auto()


@dataclass
class Basic:
    Type: int
    Address: str = "localhost"#getExternalAddress()
    Timestamp: int = datetime.now().strftime('%d.%m.%Y, %H:%M:%S')

@dataclass
class Register(Basic):
    Type: int = MsgType.REG
    Name: str = "No Name"
    TZ: str = tzlocal.get_localzone_name()

