import socket
import json
import Messages 


HOST = "127.0.0.1"  # The server's hostname or IP address
PORT = 3333  # The port used by the server

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)



def send_msg(s, msg):
    msg = Messages.asdict(msg)
    data = json.dumps(msg)+"\r\n\r\n"
    pkg = bytearray()
    pkg.extend(map(ord, data))
    s.sendall(pkg)

def recv_msg(s):
    pkg = s.recv(1024)
    data = pkg.decode()
    msg = json.loads(data)
    return msg

def register(s, name):
    send_msg(s, Messages.Register(Name=name))
    return recv_msg(s)['Type'] == Messages.MsgType.OK


def main():
    s.connect((HOST, PORT))
    print(register(s, 'Alan'))
    
    

if __name__ == "__main__":
    main()