package main

import (
	"encoding/json"
)

type Message interface{}

type Basic struct {
	Type      int
	Address   string
	Timestamp int
}

type Move struct {
	Basic
	X int
	Y int
}

type Ok struct {
	Basic
	Text string
}

type Fail struct {
	Basic
	ErrorId   int
	ErrorText string
	TailLog   []string
}

func JsonEncode(msg Message) []byte {
	pkg, _ := json.Marshal(msg)
	return []byte(string(pkg))
}

func JsonDecode(data string) Message {
	msg := Basic{}

	json.Unmarshal([]byte(data), &msg)
	switch msg.Type {
	case CON:
		msg := Connect{}
		json.Unmarshal([]byte(data), &msg)
	case BYE:
		msg := Disconnect{}
		json.Unmarshal([]byte(data), &msg)
	case OK:
		msg := Ok{}
		json.Unmarshal([]byte(data), &msg)
	case FAIL:
		msg := Fail{}
		json.Unmarshal([]byte(data), &msg)
	case MOVE:
		msg := Move{}
		json.Unmarshal([]byte(data), &msg)
	}

	return msg
}

type Connect struct {
	Basic
	Name string
	TZ   string
}

type Disconnect struct {
	Basic
	Reason string
}

type Information struct {
	Basic
	Content string
}

const (
	CON  = iota
	BYE  = iota
	OK   = iota
	FAIL = iota
	MOVE = iota
	INFO = iota
)
