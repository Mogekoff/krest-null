package main

import (
	"bufio"
	"io"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
)

const (
	StopCharacter = "\r\n\r\n"
	PORT          = 3333
	PKG_SIZE      = 1024
)

func SocketServer(port int) {

	listen, err := net.Listen("tcp4", ":"+strconv.Itoa(port))

	if err != nil {
		log.Fatalf("Socket listen port %d failed,%s", port, err)
		os.Exit(1)
	}

	defer listen.Close()

	log.Printf("Begin listen port: %d", port)

	var playerChannels = []chan Message{}

	for i := 0; i < 2; i++ {
		conn, err := listen.Accept()
		if err != nil {
			log.Fatalln(err)
			continue
		}
		ch := make(chan Message)
		playerChannels = append(playerChannels, ch)
		go handler(conn, ch)
	}
	game_session(playerChannels)
}
func connect(ch chan Message) Player {
	var msg = (<-ch).(Connect)
	ch <- Ok{Text: "Connected"}
	return Player{msg.Name, msg.Address, 'x', ch}
}
func game_session(playerChannels []chan Message) {
	var players = []Player{connect(playerChannels[0]), connect(playerChannels[1])}
	cfg := Config{players, 3}
	engine := Engine{cfg, [][]rune{}}
	engine.GenerateField()
	engine.Play()
}

func handler(conn net.Conn, ch chan Message) {

	defer conn.Close()

	var (
		buf = make([]byte, PKG_SIZE)
		r   = bufio.NewReader(conn)
		w   = bufio.NewWriter(conn)
	)

ILOOP:
	for {
		n, err := r.Read(buf)
		data := string(buf[:n])

		switch err {
		case io.EOF:
			break ILOOP
		case nil:

			var msg = JsonDecode(data)
			ch <- msg

			if isTransportOver(data) {
				break ILOOP
			}

		default:
			log.Fatalf("Receive data failed:%s", err)
			return
		}

	}
	msg := <-ch
	pkg := JsonEncode(msg)

	w.Write(pkg)
	w.Flush()
	//log.Printf("Send: %s", string(pkg))

}

func isTransportOver(data string) (over bool) {
	over = strings.HasSuffix(data, "\r\n\r\n")
	return
}

func main() {

	SocketServer(PORT)

}
