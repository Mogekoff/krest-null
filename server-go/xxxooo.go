package main

import (
	"fmt"
)

type Player struct {
	Name      string
	Address   string
	Character rune
	Channel   chan Message
}

type Config struct {
	Players  []Player
	GameMode int
}

type Engine struct {
	Config Config
	Field  [][]rune
}

func (e *Engine) PrintField() {
	size := e.Config.GameMode
	field := e.Field
	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			fmt.Printf("%v\t", string(field[i][j]))
		}
		fmt.Println()
		fmt.Println()
	}
}

func (e *Engine) GenerateField() {
	size := e.Config.GameMode
	field := make([][]rune, size)
	for i := 0; i < size; i++ {
		field[i] = make([]rune, size)
		for j := 0; j < size; j++ {
			field[i][j] = '*'
		}
	}
	e.Field = field
}
func (e *Engine) IsLegitMove(x, y int) bool {
	if 0 <= x && x <= 2 && 0 <= y && y <= 2 && e.Field[x][y] == '*' {
		return true
	}
	return false
}
func (e *Engine) IsPlayerWon(player Player) bool {
	char := player.Character

	switch {
	case e.Field[0][0] == char && e.Field[0][1] == char && e.Field[0][2] == char:
		fallthrough
	case e.Field[1][0] == char && e.Field[1][1] == char && e.Field[1][2] == char:
		fallthrough
	case e.Field[2][0] == char && e.Field[2][1] == char && e.Field[2][2] == char:
		fallthrough
	case e.Field[0][0] == char && e.Field[1][0] == char && e.Field[2][0] == char:
		fallthrough
	case e.Field[0][1] == char && e.Field[1][1] == char && e.Field[2][1] == char:
		fallthrough
	case e.Field[0][2] == char && e.Field[1][2] == char && e.Field[2][2] == char:
		fallthrough
	case e.Field[0][0] == char && e.Field[1][1] == char && e.Field[2][2] == char:
		fallthrough
	case e.Field[0][2] == char && e.Field[1][1] == char && e.Field[2][0] == char:
		return true
	default:
		return false
	}
}

func (p *Player) AskMove() (x int, y int) {
	fmt.Printf("Игрок %s, Ваш ход: ", p.Name)
	fmt.Scan(&x, &y)
	return x, y
}

func (engine *Engine) Play() {
	// var players []Player

	// firstPlayer, secondPlayer := Player{"alesha", "local", 'x'}, Player{"stepa", "local", 'o'}
	// players = append(players, firstPlayer, secondPlayer)

	// cfg := Config{players, 3}
	// engine := Engine{cfg, [][]rune{}}
	// engine.GenerateField()

	for i := 0; ; {
		engine.PrintField()
		currentPlayer := engine.Config.Players[i]
		move_x, move_y := currentPlayer.AskMove()
		if engine.IsLegitMove(move_x, move_y) {
			engine.Field[move_x][move_y] = currentPlayer.Character
			i++
		} else {
			continue
		}
		if engine.IsPlayerWon(currentPlayer) {
			engine.PrintField()
			fmt.Printf("Игрок %s победил! Поздравляем.", currentPlayer.Name)
			return
		}
		if i == len(engine.Config.Players) {
			i = 0
		}
	}
}
